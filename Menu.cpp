#include "Menu.h"


Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

cimg_library::CImg<unsigned char>* Menu::getBoard()
{
	return _board;
}

cimg_library::CImgDisplay* Menu::getDisp()
{
	return _disp;
}