#include "Shape.h"

Shape::Shape(const string& name, const string& type)
{
	_name = name;
	
	_type = type;
}

void Shape::printDetails() const
{
	cout <<_type << "\t" << _name << "\t" << getArea() << "\t" << getPerimeter() << endl;
}

string Shape::getName() const
{
	return _name;
}

string Shape::getType() const
{
	return _type;
}