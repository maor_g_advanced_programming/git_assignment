#include "ShapeNode.h"

void insertAtEnd(ShapeNode ** head, Shape * node)
{
	ShapeNode * curr = *head;
	ShapeNode * newNode = new ShapeNode();
	newNode->data = node;

	if (!*head) // empty list!
	{
		*head = newNode;
	}
	else
	{
		while (curr->next) // while the next is NOT NULL (when next is NULL - that is the last node)
		{
			curr = curr->next;
		}

		curr->next = newNode;
		newNode->next = NULL;
	}
}

void printList(ShapeNode * head)
{
	int i = SET;

	system("cls");
	
	while (head)
	{
		cout << "Enter " << i << " for " << head->data->getName() << "(" << head->data->getType() << ")" << endl;
		head = head->next;
		i++;
	}
}

void freeList(ShapeNode ** head)
{
	ShapeNode * temp = NULL;
	ShapeNode * curr = *head;

	if (curr)
	{
		temp = curr;
		curr = curr->next;
		delete temp->data;
		free(temp);
		freeList(&curr);
	}

	*head = NULL;
}

void removeFromList(ShapeNode ** head, int index)
{
	ShapeNode * curr = *head;
	ShapeNode * temp = NULL;
	int i = SET;

	if (index == FIRST)
	{
		(*head) = (*head)->next;
		
		curr->next = NULL; // I don't want that the free list will delete all the list.
		freeList(&curr);
	}
	else
	{
		while ((i + NEXT) != index && curr)
		{
			curr = curr->next;

			i++;
		}

		temp = curr->next;
		curr->next = curr->next->next;

		temp->next = NULL; // I don't want that the free list will delete all the list.
		freeList(&temp);
	}
}

Shape* getNode(ShapeNode * head, int index)
{
	ShapeNode * curr = head;
	int i = SET;

	while (i != index && curr)
	{
		curr = curr->next;

		i++;
	}

	return (curr->data);
}

void drawAllShapes(ShapeNode * head, Menu * menu)
{
	while (head)
	{
		head->data->draw(*(menu->getDisp()), *(menu->getBoard()));

		head = head->next;
	}
}