#include "Menu.h"
#include "ShapeNode.h"

#define SET 0
#define TYPE_CIRCLE "Circle"
#define TYPE_ARROW "Arrow"
#define TYPE_TRIANGLE "Triangle"
#define TYPE_RECTANGLE "Rectangle"

enum MainMenu
{
	ADD,
	INFO,
	DELETE_SHAPE,
	EXIT
};

enum ADD_SHAPE
{
	CIRCLE,
	ARROW,
	TRIANGLE,
	RECTANGLE
};

enum INFO_MENU
{
	MOVE,
	DETALIS,
	REMOVE
};

using std::cout;
using std::cin;
using std::endl;

void main()
{
	int select = SET;
	int x = SET;
	int y = SET;
	double len1 = SET;
	double len2 = SET;
	string name = "";
	ShapeNode* head = NULL;
	Menu * menu = new Menu();
	int i = SET;

	do
	{
		system("cls");
		
		cout << "Enter " << ADD << " to add a new shape." << endl;
		cout << "Enter " << INFO << " to modify or get information from a current shape." << endl;
		cout << "Enter " << DELETE_SHAPE << " to delete all of the shapes." << endl;
		cout << "Enter " << EXIT << " to exit." << endl;

		cin >> select;

		if (select == ADD)
		{
			system("cls");
			
			cout << "Enter " << CIRCLE << " to add a circle." << endl;
			cout << "Enter " << ARROW << " to add an arrow." << endl;
			cout << "Enter " << TRIANGLE << " to add a triangle." << endl;
			cout << "Enter " << RECTANGLE << " to add a rectangle." << endl;

			cin >> select;

			if (select == CIRCLE)
			{
				cout << "Please enter X:" << endl;
				cin >> x;

				cout << "Please enter Y:" << endl;
				cin >> y;

				Point center(x, y);

				cout << "Please enter radius:" << endl;
				cin >> len1;

				cout << "Please enter the name of the shape:" << endl;
				cin >> name;

				Circle* newCircle = new Circle(center, len1, TYPE_CIRCLE, name);
				insertAtEnd(&head, newCircle);
				newCircle->draw(*(menu->getDisp()), *(menu->getBoard()));
			}
			else if (select == ARROW)
			{
				cout << "Enter the X of point number: 1" << endl;
				cin >> x;

				cout << "Enter the Y of point number: 1" << endl;
				cin >> y;

				Point one(x, y);

				cout << "Enter the X of point number: 2" << endl;
				cin >> x;

				cout << "Enter the Y of point number: 2" << endl;
				cin >> y;

				Point two(x, y);

				cout << "Enter the name of the shape:" << endl;
				cin >> name;

				Arrow* newArrow = new Arrow(one, two, TYPE_ARROW, name);
				insertAtEnd(&head, newArrow);
				newArrow->draw(*(menu->getDisp()), *(menu->getBoard()));
			}
			else if (select == TRIANGLE)
			{
				cout << "Enter the X of point number: 1" << endl;
				cin >> x;

				cout << "Enter the Y of point number: 1" << endl;
				cin >> y;

				Point one(x, y);

				cout << "Enter the X of point number: 2" << endl;
				cin >> x;

				cout << "Enter the Y of point number: 2" << endl;
				cin >> y;

				Point two(x, y);

				cout << "Enter the X of point number: 3" << endl;
				cin >> x;

				cout << "Enter the Y of point number: 3" << endl;
				cin >> y;

				Point three(x, y);

				cout << "Enter the name of the shape:" << endl;
				cin >> name;

				try
				{
					Triangle* newTriangle = new Triangle(one, two, three, TYPE_TRIANGLE, name);
					insertAtEnd(&head, newTriangle);
					newTriangle->draw(*(menu->getDisp()), *(menu->getBoard()));
				}
				catch (const invalid_argument& inva)
				{
					cout << inva.what() << endl;
				}
			}
			else if (select == RECTANGLE)
			{
				cout << "Enter the X of the to left corner:" << endl;
				cin >> x;

				cout << "Enter the Y of the top left corner:" << endl;
				cin >> y;

				Point leftCorner(x, y);

				cout << "Please enter the length of the shape:" << endl;
				cin >> len1;

				cout << "Please enter the width of the shape:" << endl;
				cin >> len2;

				cout << "Enter the name of the shape:" << endl;
				cin >> name;

				try
				{
					myShapes::Rectangle* newRectangle = new myShapes::Rectangle(leftCorner, len1, len2, TYPE_RECTANGLE, name);
					insertAtEnd(&head, newRectangle);
					newRectangle->draw(*(menu->getDisp()), *(menu->getBoard()));
				}
				catch (const invalid_argument& inva)
				{
					cout << inva.what() << endl;
				}
			}
			else
			{
				cout << "Worng input." << endl;
			}

			select = SET;
		}
		else if (select == INFO)
		{
			if (head)
			{
				printList(head);

				cin >> i;

				Shape * currShape = getNode(head, i);

				cout << "Enter " << MOVE << " to move the shape" << endl;
				cout << "Enter " << DETALIS << " to get its details." << endl;
				cout << "Enter " << REMOVE << " to remove the shape." << endl;
				cin >> select;

				if (select == MOVE)
				{
					system("cls");
					
					cout << "Please enter the X moving scale: ";
					cin >> x;

					cout << "Please enter the Y moving scale: ";
					cin >> y;

					Point other(x, y);

					currShape->clearDraw(*(menu->getDisp()), *(menu->getBoard()));
					currShape->move(other);
					drawAllShapes(head, menu);
				}
				else if (select == DETALIS)
				{
					currShape->printDetails();
				}
				else if (select == REMOVE)
				{
					currShape->clearDraw(*(menu->getDisp()), *(menu->getBoard()));
					removeFromList(&head, i);
					drawAllShapes(head, menu);
				}
				else
				{
					cout << "Worng input." << endl;
				}

				select = SET;
			}
		}
		else if (select == DELETE_SHAPE)
		{
			freeList(&head);
		}
		else if (select == EXIT)
		{
			// do nothing
		}
		else
		{
			cout << "Worng input." << endl;
		}

	} while (select != EXIT);
}