#pragma once

#include "Shape.h"
#include "Menu.h"

#define SET 0
#define FIRST 0
#define NEXT 1

struct ShapeNode
{
	Shape* data;
	ShapeNode* next;
}typedef ShapeNode;

/*
Function will print a list of shapes
input: the list (the first shape)
output:
none
*/
void printList(ShapeNode * head);

/*
Function will add a person to the list
input:
firstNode - the first node of the list
newNode - the new person to add to the list
output:
none
*/
void insertAtEnd(ShapeNode ** head, Shape * newNode);

/*
Function will free all memory of a list of shapes
input:
a list of shapes
output:
none
*/
void freeList(ShapeNode ** head);

/*
function will remove a shape from the list, it will get it's index at the list from the user
input: a pointer the the list head
output: none
*/
void removeFromList(ShapeNode ** head, int index);

/*
function will return a node from the list by index
input: the list, and a index
output: a Shape pointer
*/
Shape* getNode(ShapeNode * head, int index);

/*
function will draw all the shapes in the input list
input: a list, and a menu to draw on
output: none
*/
void drawAllShapes(ShapeNode * head, Menu * menu);