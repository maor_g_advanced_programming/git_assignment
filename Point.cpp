#include "Point.h"

Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}

Point::Point(const Point& other)
{
	_x = other._x;

	_y = other._y;
}

Point::~Point()
{

}

Point Point::operator+(const Point& other) const
{
	Point p(*this);

	p._x += other._x;
	p._y += other._y;

	return p;
}

Point& Point::operator+=(const Point& other)
{
	(*this) = (*this) + other;

	return (*this);
}

double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

double Point::distance(const Point& other) const
{
	return sqrt(pow(_x - other._x, USE_TO_POW) + pow(_y - other._y, USE_TO_POW));
}