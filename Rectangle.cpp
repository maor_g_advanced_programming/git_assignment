#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name) : Polygon(type, name), _topLeft(a)
{
	if (length > POSITIVE && width > POSITIVE)
	{
		_lenght = length;
		_width = width;
	}
	else
	{
		throw invalid_argument("lenght or width are negative. can't create the Rectangle");
	}

	_points.push_back(a);

	Point b(a.getX() + length, a.getY() + width);
	_points.push_back(b);
}

myShapes::Rectangle::~Rectangle()
{

}

double myShapes::Rectangle::getArea() const
{
	return _lenght * _width;
}

double myShapes::Rectangle::getPerimeter() const
{
	return _lenght + _lenght + _width + _width;
}

void myShapes::Rectangle::move(const Point& other)
{
	_topLeft += other;

	Point buttomRight(_topLeft.getX() + _lenght, _topLeft.getY() + _width);
	
	_points[FIRST] = _topLeft;
	_points[SECOND] = buttomRight;
}

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


