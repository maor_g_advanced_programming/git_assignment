#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) : Polygon(type, name), _a(a), _b(b), _c(c)
{
	// m = (y1 - y2) / (x1 - x2)
	double m1 = (a.getY() - b.getY()) / (a.getX() - b.getX());
	double m2 = (b.getY() - c.getY()) / (b.getX() - c.getX());


	if (m1 == m2) // if the incline are the same so there is a line
	{
		throw invalid_argument("The points entered create a line.");
	}

	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
}

Triangle::~Triangle()
{

}

double Triangle::getArea() const
{
	// to see the formola I used:
	// http://www.dummies.com/education/math/algebra/finding-the-area-of-a-triangle-using-its-coordinates/
	double semiPerimeter = getPerimeter() / HALF;
	double a = _a.distance(_b);
	double b = _a.distance(_c);
	double c = _b.distance(_c);

	return sqrt(semiPerimeter * (semiPerimeter - a) * (semiPerimeter - b) * (semiPerimeter - c));
}

double Triangle::getPerimeter() const
{
	return _a.distance(_b) + _a.distance(_c) + _b.distance(_c);
}

void Triangle::move(const Point& other)
{
	_a += other;
	_b += other;
	_c += other;

	_points[FIRST] = _a;
	_points[SECOND] = _b;
	_points[THIRD] = _c;
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
